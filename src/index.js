import express from 'express';
import initServer from './config/initServer';

if (module === require.main) {
    const app = express();
    initServer(app);
}