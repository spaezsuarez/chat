import express from 'express';
import { resolve } from 'path';

const initServer = (app) => {
    
    //Configurando Archivos estaticos
    app.use(express.static(resolve('public')));

    app.get('/',(request,response) => {
        response.render('index.html');
    });
    
    app.listen(3000,() => {
        console.log('Servidor Corriendo en puerto 3000');
    });
}

export default initServer;